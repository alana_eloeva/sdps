import 'package:flutter/material.dart';

class Year {
  final String name;
  bool selected;

  Year({@required this.name, @required this.selected});
}

List<Year> getYears() {
  return [
    Year(name: '2001', selected: false),
    Year(name: '2002', selected: false),
    Year(name: '2003', selected: false),
    Year(name: '2004', selected: false),
    Year(name: '2005', selected: false),
    Year(name: '2006', selected: false),
    Year(name: '2007', selected: false),
    Year(name: '200i', selected: false),
    Year(name: '2009', selected: false),
  ];
}
