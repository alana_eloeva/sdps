

class BaseResponse {

  bool success;
  String error;

  BaseResponse({this.success, this.error});

  BaseResponse.fromJson(Map<String, dynamic> json)
      : success = json['success'],
        error = json['error'] == null ? '' : json['error'];

}