import 'package:flutter/material.dart';

class Algorithm {
  final String name;
  bool selected;

  Algorithm({@required this.name, @required this.selected});
}

List<Algorithm> getAlgorithms() {
  return [
    Algorithm(name: 'Алгоритм1', selected: false),
    Algorithm(name: 'Алгоритм2', selected: false),
    Algorithm(name: 'Алгоритм3', selected: false),
    Algorithm(name: 'Алгоритм4', selected: false),
  ];
}
