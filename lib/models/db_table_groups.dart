import 'package:hive/hive.dart';
part 'db_table_groups.g.dart';

@HiveType(typeId: 0)
class CategoriesResponse {
  CategoriesResponse({
    this.success,
    this.data,
  });

  @HiveField(0)
  bool success;
  @HiveField(1)
  List<Category> data;

  factory CategoriesResponse.fromJson(Map<String, dynamic> json) =>
      CategoriesResponse(
        success: json["success"],
        data:
            List<Category>.from(json["data"].map((x) => Category.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "success": success,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

@HiveType(typeId: 1)
class Category {
  Category({this.id, this.name, this.tables, this.normalize, this.selected});

  @HiveField(0)
  int id;
  @HiveField(1)
  String name;
  @HiveField(2)
  bool normalize;
  @HiveField(3)
  bool selected;
  @HiveField(4)
  List<Table> tables;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        id: json["id"],
        name: json["name"],
        tables: List<Table>.from(json["tables"].map((x) => Table.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "tables": List<dynamic>.from(tables.map((x) => x.toJson())),
      };
}

@HiveType(typeId: 2)
class Table {
  Table({
    this.id,
    this.name,
    this.description,
    this.tableGroup,
  });

  @HiveField(0)
  int id;
  @HiveField(1)
  String name;
  @HiveField(3)
  String description;
  @HiveField(4)
  int tableGroup;

  factory Table.fromJson(Map<String, dynamic> json) => Table(
        id: json["id"],
        name: json["name"],
        description: json["description"] == null ? null : json["description"],
        tableGroup: json["table_group"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description == null ? null : description,
        "table_group": tableGroup,
      };
}

getTables() {
  return [
    Category(name: 'Таблица1', selected: false, normalize: false),
    Category(name: 'Таблица2', selected: false, normalize: false),
    Category(name: 'Таблица3', selected: false, normalize: false),
    Category(name: 'Таблица4', selected: false, normalize: false),
  ];
}
