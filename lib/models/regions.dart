import 'package:flutter/material.dart';

class Region {
  final String name;
  bool selected;

  Region({@required this.name, @required this.selected});
}

List<Region> getRegions() {
  return [
    Region(
      name: "Республика Дагестан",
      selected: false,
    ),
    Region(
      name: "Республика Ингушетия",
      selected: false,
    ),
    Region(
      name: "Кабардино-Балкарская Республика",
      selected: false,
    ),
    Region(
      name: "Карачаево-Черкесская Республика",
      selected: false,
    ),
    Region(
      name: "Республика Северная Осетия – Алания",
      selected: false,
    ),
    Region(
      name: "Чеченская Республика",
      selected: false,
    ),
    Region(
      name: "Ставропольский край",
      selected: false,
    ),
  ];
}
