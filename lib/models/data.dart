// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

DataResponse welcomeFromJson(String str) => DataResponse.fromJson(json.decode(str));

String welcomeToJson(DataResponse data) => json.encode(data.toJson());

class DataResponse {
  DataResponse({
    this.success,
    this.data,
  });

  bool success;
  List<Datum> data;

  factory DataResponse.fromJson(Map<String, dynamic> json) => DataResponse(
    success: json["success"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.period,
    this.value,
    this.tableType,
    this.region,
    this.source,
    this.valueName,
  });

  Period period;
  String value;
  TableType tableType;
  Region region;
  dynamic source;
  dynamic valueName;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    period: Period.fromJson(json["period"]),
    value: json["value"],
    tableType: TableType.fromJson(json["table_type"]),
    region: Region.fromJson(json["region"]),
    source: json["source"],
    valueName: json["value_name"],
  );

  Map<String, dynamic> toJson() => {
    "period": period.toJson(),
    "value": value,
    "table_type": tableType.toJson(),
    "region": region.toJson(),
    "source": source,
    "value_name": valueName,
  };
}

class Period {
  Period({
    this.id,
    this.name,
  });

  int id;
  String name;

  factory Period.fromJson(Map<String, dynamic> json) => Period(
    id: json["id"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
  };
}

class Region {
  Region({
    this.code,
    this.name,
    this.district,
  });

  int code;
  String name;
  int district;

  factory Region.fromJson(Map<String, dynamic> json) => Region(
    code: json["code"],
    name: json["name"],
    district: json["district"],
  );

  Map<String, dynamic> toJson() => {
    "code": code,
    "name": name,
    "district": district,
  };
}

class TableType {
  TableType({
    this.id,
    this.name,
    this.description,
    this.tableGroup,
  });

  int id;
  String name;
  String description;
  int tableGroup;

  factory TableType.fromJson(Map<String, dynamic> json) => TableType(
    id: json["id"],
    name: json["name"],
    description: json["description"],
    tableGroup: json["table_group"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "description": description,
    "table_group": tableGroup,
  };
}
