// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'db_table_groups.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CategoriesResponseAdapter extends TypeAdapter<CategoriesResponse> {
  @override
  final int typeId = 0;

  @override
  CategoriesResponse read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CategoriesResponse(
      success: fields[0] as bool,
      data: (fields[1] as List)?.cast<Category>(),
    );
  }

  @override
  void write(BinaryWriter writer, CategoriesResponse obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.success)
      ..writeByte(1)
      ..write(obj.data);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CategoriesResponseAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class CategoryAdapter extends TypeAdapter<Category> {
  @override
  final int typeId = 1;

  @override
  Category read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Category(
      id: fields[0] as int,
      name: fields[1] as String,
      tables: (fields[4] as List)?.cast<Table>(),
      normalize: fields[2] as bool,
      selected: fields[3] as bool,
    );
  }

  @override
  void write(BinaryWriter writer, Category obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.normalize)
      ..writeByte(3)
      ..write(obj.selected)
      ..writeByte(4)
      ..write(obj.tables);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CategoryAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class TableAdapter extends TypeAdapter<Table> {
  @override
  final int typeId = 2;

  @override
  Table read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Table(
      id: fields[0] as int,
      name: fields[1] as String,
      description: fields[3] as String,
      tableGroup: fields[4] as int,
    );
  }

  @override
  void write(BinaryWriter writer, Table obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(3)
      ..write(obj.description)
      ..writeByte(4)
      ..write(obj.tableGroup);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TableAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
