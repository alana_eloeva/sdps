import 'package:flutter/material.dart';
import 'package:sdps/constants/app_strings.dart';
import 'package:sdps/constants/app_styles.dart';
import 'package:sdps/main.dart';
import 'package:sdps/ui/widgets/parent_scaffold.dart';
import 'package:sdps/utils/route_names.dart';
import 'package:sdps/viewmodels/analysis/data_analysis_data_viewmodel.dart';
import 'package:sdps/viewmodels/analysis/data_analysis_viewmodel.dart';
import 'package:stacked/stacked.dart';

class DataAnalysisParamsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<DataAnalysisParamsViewModel>.reactive(
      viewModelBuilder: () => DataAnalysisParamsViewModel(),
      builder: (context, model, builder) {
        return MyScaffold(
          route: DataAnalysisParamsRoute,
          body: ParentContainer(
            title: appbar_analysis,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(bottom: 14),
                  child: Text(
                    'Выбор параметров алгоритма',
                    textAlign: TextAlign.start,
                    style: textStyle_header2,
                  ),
                ),

                SizedBox(
                  height: 20,
                ),

                Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(bottom: 24),
                  width: 360,
                  child: TextField(
                    onChanged: (value) {},
                    decoration: setTextFieldDecoration(
                      hint: 'Пармертр1',
                    ),
                  ),
                ),

                SizedBox(
                  height: 10,
                ),

                Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(bottom: 24),
                  width: 360,
                  child: TextField(
                    onChanged: (value) {},
                    decoration: setTextFieldDecoration(
                      hint: 'Пармертр2',
                    ),
                  ),
                ),

                SizedBox(
                  height: 10,
                ),

                Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(bottom: 24),
                  width: 360,
                  child: TextField(
                    onChanged: (value) {},
                    decoration: setTextFieldDecoration(
                      hint: 'Пармертр3',
                    ),
                  ),
                ),

                SizedBox(
                  height: 10,
                ),

                Container(
                  margin: EdgeInsets.only(left: 8, right: 8),
                  padding: EdgeInsets.only(left: 24, right: 24),
                  height: 40,
                  width: 146,
                  alignment: Alignment.center,
                  decoration: buttonDecoration,
                  child: Text(
                    'Применить',
                    style: TextStyle(color: Colors.white, fontSize: 14),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

