import 'package:flutter/material.dart';
import 'package:provider_architecture/_provider_widget.dart';
import 'package:sdps/constants/app_colors.dart';
import 'package:sdps/constants/app_strings.dart';
import 'package:sdps/constants/app_styles.dart';
import 'package:sdps/main.dart';
import 'package:sdps/ui/widgets/parent_scaffold.dart';
import 'package:sdps/utils/route_names.dart';
import 'package:sdps/viewmodels/analysis/data_analysis_data_viewmodel.dart';
import 'package:stacked/stacked.dart';

class DataAnalysisDataView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<DataAnalysisDataViewModel>.reactive(
      viewModelBuilder: () => DataAnalysisDataViewModel(),
      builder: (context, model, builder) {
        return MyScaffold(
          route: DataAnalysisDataRoute,
          body: ParentContainer(
            title: appbar_analysis_data,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                SearchTableWidget(),
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        'Таблица',
                        textAlign: TextAlign.start,
                        style: textStyle_header3_grey,
                      ),
                    ),
                    Expanded(
                      child: Text(
                        'Выбор',
                        textAlign: TextAlign.start,
                        style: textStyle_header3_grey,
                      ),
                    ),
                    Expanded(
                      child: Text(
                        'Нормировать',
                        textAlign: TextAlign.start,
                        style: textStyle_header3_grey,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  height: 1,
                  color: lightGreyColor2,
                ),
                Expanded(child: CheckedListView()),
              ],
            ),
          ),
        );
      },
    );
  }
}

class SearchTableWidget extends ProviderWidget<DataAnalysisDataViewModel> {
  @override
  Widget build(BuildContext context, DataAnalysisDataViewModel model) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(bottom: 14),
              child: Text(
                'Поиск таблицы',
                textAlign: TextAlign.start,
                style: textStyle_header2,
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(bottom: 24),
              width: 360,
              child: TextField(
                onChanged: (value) {},
                decoration: setTextFieldDecoration(
                  hint: 'Поиск',
                ),
              ),
            ),
          ],
        ),
        Row(
          children: [
            InkWell(
              onTap: () {
                Navigator.pushNamed(context, DataAnalysisPeriodsRoute);
              },
              child: Container(
                margin: EdgeInsets.only(left: 8, right: 8),
                padding: EdgeInsets.only(left: 24, right: 24),
                height: 40,
                alignment: Alignment.center,
                decoration: buttonDecoration,
                child: Text(
                  'Далее',
                  style: TextStyle(color: Colors.white, fontSize: 14),
                ),
              ),
            ),
          ],
        )
      ],
    );
  }
}

class CheckedListView extends ProviderWidget<DataAnalysisDataViewModel> {
  @override
  Widget build(BuildContext context, DataAnalysisDataViewModel model) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: model.tables.length,
      itemBuilder: (BuildContext context, int index) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              height: 60,
              alignment: Alignment.centerLeft,
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        model.tables[index].name,
                        textAlign: TextAlign.start,
                        style: textStyle_header3,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      alignment: Alignment.centerLeft,
                      child: Checkbox(
                        value: model.tables[index].selected,
                        checkColor: Colors.white,
                        activeColor: blueColor2,
                        onChanged: (value) {
                          model.onCheckRegion(index, value);
                        },
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      alignment: Alignment.centerLeft,
                      child: Checkbox(
                        value: model.tables[index].normalize,
                        checkColor: Colors.white,
                        activeColor: blueColor2,
                        onChanged: (value) {
                          model.onCheckNormalize(index, value);
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 1,
              color: lightGreyColor2,
            )
          ],
        );
      },
    );
  }
}
