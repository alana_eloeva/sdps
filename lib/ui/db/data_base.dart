import 'package:flutter/material.dart';
import 'package:sdps/main.dart';
import 'package:sdps/ui/widgets/parent_scaffold.dart';
import 'package:sdps/utils/route_names.dart';

class DataBase extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      route: DataBaseRoute,
      body: ParentContainer(
        child: Container(),
      ),
    );
  }
}
