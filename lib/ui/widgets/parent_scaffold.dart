import 'package:flutter/material.dart';
import 'package:sdps/constants/app_colors.dart';
import 'package:sdps/constants/app_styles.dart';

class ParentContainer extends StatelessWidget {
  final Widget child;
  final String title;

  final bool isBackButton;

  ParentContainer(
      {@required this.child,
      @required this.title,
      this.isBackButton = true});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 20,
        ),

        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Visibility(
              visible: isBackButton,
              child: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
            ),
            SizedBox(
              width: 12,
            ),
            Text(
              title,
              textAlign: TextAlign.start,
              style: textStyle_header1,
            )
          ],
        ),
        Expanded(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            margin: EdgeInsets.all(16),
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: lightGreyColor2),
                borderRadius: BorderRadius.all(Radius.circular(8))),
            child: child,
          ),
        ),
      ],
    );
  }
}
