import 'package:flutter/material.dart';
import 'package:lazy_data_table_plus/lazy_data_table_plus.dart';
import 'package:provider_architecture/_provider_widget.dart';
import 'package:sdps/constants/app_colors.dart';
import 'package:sdps/constants/app_strings.dart';
import 'package:sdps/constants/app_styles.dart';
import 'package:sdps/main.dart';
import 'package:sdps/ui/widgets/parent_scaffold.dart';
import 'package:sdps/utils/route_names.dart';
import 'package:sdps/viewmodels/adding_data/step_second_viewmodel.dart';
import 'package:stacked/stacked.dart';

class AddingDataSecondStepArguments {
  final List<String> regions;
  final List<String> fields;

  AddingDataSecondStepArguments(
      {@required this.regions, @required this.fields});
}

class AddingDataSecondStepView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context).settings.arguments
        as AddingDataSecondStepArguments;
    return ViewModelBuilder<AddingDataSecondStepViewModel>.reactive(
      viewModelBuilder: () => AddingDataSecondStepViewModel(
          row: args.regions.length, column: args.fields.length),
      builder: (context, model, builder) {
        return MyScaffold(
          route: AddingDataSecondStepRoute,
          body: ParentContainer(
              title: appbar_adding_data_step2,
              child: Column(
            children: [
              PeriodWidget(),
              Expanded(
                child: LazyDataTable(
                  rows: args.regions.length,
                  columns: args.fields.length,
                  tableDimensions: LazyDataTableDimensions(
                    cellHeight: 50,
                    cellWidth: 140,
                    columnHeaderHeight: 70,
                    rowHeaderWidth: 230,
                  ),
                  tableTheme: LazyDataTableTheme(
                    columnHeaderBorder: Border(
                        bottom: BorderSide(
                      color: lightGreyColor2,
                      width: 1,
                    )),
                    rowHeaderBorder: Border(
                        bottom: BorderSide(
                      color: lightGreyColor2,
                      width: 1,
                    )),
                    cellBorder: Border(
                        bottom: BorderSide(
                      color: lightGreyColor2,
                      width: 1,
                    )),
                    cornerBorder: Border(
                        bottom: BorderSide(
                      color: lightGreyColor2,
                      width: 1,
                    )),
                    columnHeaderColor: Colors.white60,
                    rowHeaderColor: Colors.white60,
                    cellColor: Colors.white,
                    cornerColor: Colors.white38,
                  ),
                  columnHeaderBuilder: (i) =>
                      Center(child: Text(args.fields[i])),
                  rowHeaderBuilder: (i) => Padding(
                      padding: EdgeInsets.all(4),
                      child: Text(
                        args.regions[i],
                        textAlign: TextAlign.start,
                        style: textStyle_header3_grey,
                      )),
                  dataCellBuilder: (i, j) => Center(
                    child: Container(
                      height: 36,
                      width: 126,
                      margin: EdgeInsets.only(
                          left: 20, right: 20, top: 10, bottom: 10),
                      child: TextField(
                        onChanged: (value) {
                          model.onCellChanged(value, i, j);
                        },
                        decoration: new InputDecoration(
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 20),
                          enabledBorder: new OutlineInputBorder(
                              borderSide:
                                  new BorderSide(color: lightGreyColor2)),
                          disabledBorder: new OutlineInputBorder(
                              borderSide:
                                  new BorderSide(color: lightGreyColor2)),
                          border: new OutlineInputBorder(
                              borderSide:
                                  new BorderSide(color: lightGreyColor2)),
                          focusedBorder: new OutlineInputBorder(
                              borderSide:
                                  new BorderSide(color: lightGreyColor2)),
                        ),
                      ),
                    ),
                  ),
                  cornerWidget: Center(child: Text("Регионы")),
                ),
              ),
            ],
          )),
        );
      },
    );
  }
}

class PeriodWidget extends ProviderWidget<AddingDataSecondStepViewModel> {
  AddingDataSecondStepViewModel _model;

  @override
  Widget build(BuildContext context, AddingDataSecondStepViewModel model) {
    this._model = model;
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                'Заполняемый период',
                textAlign: TextAlign.start,
                style: textStyle_header2,
              ),
            ),
            InkWell(
              onTap: () {
                showDatePicker(context);
              },
              child: Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(top: 14, bottom: 14),
                child: Text(
                  model.selectedYearForShow,
                  textAlign: TextAlign.start,
                  style: textStyle_underline,
                ),
              ),
            )
          ],
        ),
        Row(
          children: [
            Container(
              margin: EdgeInsets.only(left: 8, right: 8),
              padding: EdgeInsets.only(left: 24, right: 24),
              height: 40,
              alignment: Alignment.center,
              decoration: buttonDecoration,
              child: Text(
                'Добавить новый период',
                style: TextStyle(color: Colors.white, fontSize: 14),
              ),
            ),
            InkWell(
              onTap: () {

              },
              child: Container(
                margin: EdgeInsets.only(left: 8, right: 8),
                padding: EdgeInsets.only(left: 24, right: 24),
                height: 40,
                alignment: Alignment.center,
                decoration: buttonDecoration,
                child: Text(
                  'Готово',
                  style: TextStyle(color: Colors.white, fontSize: 14),
                ),
              ),
            ),
          ],
        )
      ],
    );
  }

  showDatePicker(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext ctx) {
        return AlertDialog(
          title: Text("Выберите год"),
          content: Container(
            // Need to use container to add size constraint.
            width: 300,
            height: 300,
            child: YearPicker(
              firstDate: DateTime(DateTime.now().year - 100, 1),
              lastDate: DateTime(DateTime.now().year + 100, 1),
              initialDate: DateTime.now(),
              selectedDate: _model.selectedDate,
              onChanged: (DateTime dateTime) {
                _model.onChangedSelectedDate(dateTime);
                Navigator.pop(ctx);
              },
            ),
          ),
        );
      },
    );
  }
}
