import 'package:flutter/material.dart';
import 'package:provider_architecture/_provider_widget.dart';
import 'package:sdps/constants/app_colors.dart';
import 'package:sdps/constants/app_strings.dart';
import 'package:sdps/constants/app_styles.dart';
import 'package:sdps/constants/constants.dart';
import 'package:sdps/ui/adding_data/step_second.dart';
import 'package:sdps/ui/widgets/parent_scaffold.dart';
import 'package:sdps/utils/route_names.dart';
import 'package:sdps/viewmodels/adding_data/step_first_viewmodel.dart';
import 'package:stacked/stacked.dart';
import 'package:sdps/main.dart';

class AddingDataFirstStepView extends StatefulWidget {
  @override
  _AddingDataFirstStepViewState createState() =>
      _AddingDataFirstStepViewState();
}

class _AddingDataFirstStepViewState extends State<AddingDataFirstStepView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<AddingDataFirstStepViewModel>.reactive(
      viewModelBuilder: () => AddingDataFirstStepViewModel(),
      builder: (context, model, builder) {
        return MyScaffold(
          route: AddingDataFirstStepRoute,
          body: ParentContainer(
            title: appbar_adding_data_step1,
            isBackButton: false,
            child: Column(
              children: [
                Expanded(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SelectTableWidget(),
                      Expanded(child: Container())
                    ],
                  ),
                ),
                SizedBox(
                  height: 12,
                ),
                Expanded(
                  child: Row(
                    children: [
                      SelectTableTypeWidget(),
                      SizedBox(
                        width: 12,
                      ),
                      AddValueWidget()
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}

class SelectTableWidget extends ProviderWidget<AddingDataFirstStepViewModel> {
  AddingDataFirstStepViewModel _model;

  @override
  Widget build(BuildContext context, AddingDataFirstStepViewModel model) {
    this._model = model;
    return Expanded(
      child: Container(
        padding: EdgeInsets.all(12),
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: lightGreyColor2),
            borderRadius: BorderRadius.all(Radius.circular(8))),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                // constraints: BoxConstraints(minWidth: 300, minHeight: 200),
                alignment: Alignment.center,
                child: Text(
                  "Укажите таблицу для заполенения",
                  textAlign: TextAlign.center,
                  style: textStyle_header2,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                children: <Widget>[
                  for (var tableType in DataSourceType.values)
                    addRadioButton(tableType)
                ],
              ),
              SizedBox(
                height: 16,
              ),
              Offstage(
                offstage: model.createNewTableTextFieldVisibility(),
                child: Theme(
                  data: Theme.of(context).copyWith(primaryColor: blueColor2),
                  child: TextField(
                    decoration: setTextFieldDecoration_withIcon(
                        hint: 'Название новой таблицы',
                        suffixIcon: Icon(Icons.add)),
                  ),
                ),
              ),
              Offstage(
                offstage: !model.createNewTableTextFieldVisibility(),
                child: Theme(
                  data: Theme.of(context).copyWith(
                    primaryColor: blueColor2,
                  ),
                  child: TextField(
                    decoration: setTextFieldDecoration_withIcon(
                        hint: 'Поиск', suffixIcon: Icon(Icons.search)),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              InkWell(
                onTap: () {
                  model.showSecondContainer();
                },
                child: Container(
                  // margin: EdgeInsets.only(top: 32),
                  height: 48,
                  width: 150,
                  alignment: Alignment.center,
                  decoration: buttonDecoration,
                  child: Text(
                    'Далее',
                    style: TextStyle(color: Colors.white, fontSize: 14),
                  ),
                ),
              )
            ]),
      ),
    );
  }

  Widget addRadioButton(DataSourceType tableType) {
    return Row(
      children: [
        Radio(
          value: tableType,
          activeColor: Colors.black,
          groupValue: _model.getCurrentDataSourceTypeValue(),
          onChanged: (DataSourceType value) {
            _model.onChangeDataSourceType(value);
          },
        ),
        Text(tableType.title()),
        SizedBox(
          width: 12,
        )
      ],
    );
  }
}

class SelectTableTypeWidget
    extends ProviderWidget<AddingDataFirstStepViewModel> {
  AddingDataFirstStepViewModel _model;

  @override
  Widget build(BuildContext context, AddingDataFirstStepViewModel model) {
    this._model = model;
    return Expanded(
        child: Visibility(
      visible: model.getSecondContainerVisibility(),
      child: Container(
        padding: EdgeInsets.all(12),
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: lightGreyColor2),
            borderRadius: BorderRadius.all(Radius.circular(8))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              alignment: Alignment.center,
              child: Text(
                "Укажите тип таблицы",
                textAlign: TextAlign.center,
                style: textStyle_header2,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            for (var tableType in TableType.values) addRadioButton(tableType),
            SizedBox(
              width: 20,
            ),
            InkWell(
              onTap: () {
                model.showThirdContainer();
              },
              child: Container(
                // margin: EdgeInsets.only(top: 32),
                height: 48,
                width: 150,
                alignment: Alignment.center,
                decoration: buttonDecoration,
                child: Text(
                  'Далее',
                  style: TextStyle(color: Colors.white, fontSize: 14),
                ),
              ),
            )
          ],
        ),
      ),
    ));
  }

  Widget addRadioButton(TableType type) {
    return Row(
      children: [
        Radio(
          value: type,
          activeColor: Colors.black,
          groupValue: _model.getCurrentTableTypeValue(),
          onChanged: (TableType value) {
            _model.onChangeTableType(value);
          },
        ),
        Text(type.title()),
        SizedBox(
          height: 16,
        ),
      ],
    );
  }
}

class AddValueWidget extends ProviderWidget<AddingDataFirstStepViewModel> {
  @override
  Widget build(BuildContext context, AddingDataFirstStepViewModel model) {
    return Expanded(
      child: Visibility(
        visible: model.getThirdContainerVisibility(),
        child: Container(
          padding: EdgeInsets.all(12),
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: lightGreyColor2),
              borderRadius: BorderRadius.all(Radius.circular(8))),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                alignment: Alignment.center,
                child: Text(
                  "Поля для заполнения",
                  textAlign: TextAlign.center,
                  style: textStyle_header2,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Column(
                children: [
                  Offstage(
                    offstage: !model.isSingleTableType(),
                    child: Theme(
                      data: Theme.of(context).copyWith(
                        primaryColor: blueColor2,
                      ),
                      child: TextField(
                        onChanged: (value) {
                          model.onSingleFieldValueChanged(value);
                        },
                        decoration: setTextFieldDecoration(
                          hint: 'Название поля',
                        ),
                      ),
                    ),
                  ),
                  Offstage(
                    offstage: model.isSingleTableType(),
                    child: Theme(
                      data: Theme.of(context).copyWith(
                        primaryColor: blueColor2,
                      ),
                      child: TextField(
                        onChanged: (value) {
                          model.onMultipleFieldsValuesChanged(value);
                        },
                        decoration: setTextFieldDecoration(
                          hint: 'Названия полей через запятую ',
                        ),
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 20,
              ),
              InkWell(
                onTap: () {
                  model.onAddFieldButtonClicked((regions, fields) {
                    Navigator.pushNamed(context, AddingDataSecondStepRoute,
                        arguments: AddingDataSecondStepArguments(
                            regions: regions, fields: fields));
                  });
                },
                child: Container(
                  // margin: EdgeInsets.only(top: 32),
                  height: 48,
                  width: 150,
                  alignment: Alignment.center,
                  decoration: buttonDecoration,
                  child: Text(
                    'Далее',
                    style: TextStyle(color: Colors.white, fontSize: 14),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
