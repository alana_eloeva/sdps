import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/material.dart';
import 'package:sdps/constants/app_colors.dart';
import 'package:sdps/constants/app_strings.dart';
import 'package:sdps/main.dart';
import 'package:sdps/models/data.dart';
import 'package:sdps/ui/widgets/parent_scaffold.dart';
import 'package:sdps/utils/route_names.dart';
import 'package:sdps/viewmodels/map/map_viewmodel.dart';
import 'package:stacked/stacked.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:syncfusion_flutter_charts/sparkcharts.dart';
import 'package:syncfusion_flutter_maps/maps.dart';
import 'dart:math' as math;

class MapView extends StatefulWidget {
  @override
  State<MapView> createState() => _MapViewState();
}

class _MapViewState extends State<MapView> {
  List<Model> _data;
  MapShapeSource _mapSource;

  refreshMapData() {}

  @override
  void initState() {
    super.initState();

    _data = const <Model>[
      Model(20, 'Чеченская республика', Color.fromRGBO(255, 215, 0, 1.0),
          '       ЧР'),
      Model(6, 'Ингушетия', Color.fromRGBO(72, 209, 204, 1.0), 'И'),
      Model(15, 'Северная Осетия - Алания', Color.fromRGBO(255, 78, 66, 1.0),
          'РСО'),
      Model(7, 'Кабардино-Балкария', Color.fromRGBO(171, 56, 224, 0.75), 'КБР'),
      Model(5, 'Дагестан', Color.fromRGBO(126, 247, 74, 0.75), 'Дагестан'),
      Model(26, 'Ставропольский край', Color.fromRGBO(79, 60, 201, 0.7),
          'Ставропольский край'),
      Model(9, 'Карачаево-Черкесия', Color.fromRGBO(99, 164, 230, 1), 'КЧР'),
    ];

    _mapSource = MapShapeSource.asset(
      'assets/skfo.json',
      shapeDataField: 'name',
      dataCount: _data.length,
      primaryValueMapper: (int index) => _data[index].state,
      dataLabelMapper: (int index) => _data[index].stateCode,
      shapeColorValueMapper: (int index) => _data[index].color,
    );
  }

  MapViewModel viewModel;

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<MapViewModel>.reactive(
      viewModelBuilder: () => MapViewModel()..getCategories(),
      builder: (context, model, builder) {
        viewModel = model;
        return MyScaffold(
          route: MapRoute,
          body: ParentContainer(
              title: appbar_map,
              isBackButton: false,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      child: Stack(
                        children: [
                          Center(
                            child: SfMaps(
                              layers: <MapShapeLayer>[
                                MapShapeLayer(
                                  source: _mapSource,
                                  showDataLabels: true,
                                  legend: const MapLegend(MapElement.shape),
                                  tooltipSettings: MapTooltipSettings(
                                      color: Colors.grey[700],
                                      strokeColor: Colors.white,
                                      strokeWidth: 2),
                                  strokeColor: Colors.white,
                                  strokeWidth: 0.5,
                                  onSelectionChanged: (int index) {
                                    model.selectRegion(_data[index].code);
                                    showFilterPopup();
                                  },
                                  shapeTooltipBuilder:
                                      (BuildContext context, int index) {
                                    return Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                        _data[index].stateCode,
                                        style: const TextStyle(
                                            color: Colors.white),
                                      ),
                                    );
                                  },
                                  dataLabelSettings: MapDataLabelSettings(
                                      textStyle: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12)),
                                ),
                              ],
                            ),
                          ),
                          // GestureDetector(
                          //     onTap: () {
                          //       showFilterPopup();
                          //     },
                          //     child: Container(
                          //         alignment: Alignment.topLeft,
                          //         child: Icon(Icons.sort)))
                        ],
                      ),
                    ),
                    // model.valuesForChart.isEmpty
                    //     ? Container()
                    //     : Container(
                    //         height: MediaQuery.of(context).size.height,
                    //         width: MediaQuery.of(context).size.width,
                    //         child: _ChartView(model: model))
                  ],
                ),
              )),
        );
      },
    );
  }

  showFilterPopup() {
    Dialog errorDialog = Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      //this right here
      child: Container(
          height: MediaQuery.of(context).size.height - 200,
          width: MediaQuery.of(context).size.width - 300,
          child: FilterView(model: viewModel)),
    );
    showDialog(
        context: context, builder: (BuildContext context) => errorDialog);
  }
}

class FilterView extends StatefulWidget {
  MapViewModel model;

  FilterView({@required this.model});

  @override
  State<StatefulWidget> createState() {
    return new HomeWidgetState();
  }
}

class HomeWidgetState extends State<FilterView>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController =
        new TabController(vsync: this, length: widget.model.tabs.length);

    _tabController.addListener(() {
      var index = _tabController.index;
      widget.model.tabPositionChanged(index);
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  int tabPosition = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12.0), color: Colors.white),
      child: new Scaffold(
        appBar: TabBar(
          isScrollable: true,
          unselectedLabelColor: Colors.grey,
          labelColor: Colors.white,
          indicatorSize: TabBarIndicatorSize.tab,
          indicator: new BubbleTabIndicator(
            indicatorHeight: 25.0,
            indicatorColor: Colors.blueAccent,
            tabBarIndicatorSize: TabBarIndicatorSize.tab,
          ),
          tabs: widget.model.tabs,
          controller: _tabController,
        ),
        body: new TabBarView(
          controller: _tabController,
          children: widget.model.tabs.map((Tab tab) {
            return SubcategoriesView(model: widget.model);
          }).toList(),
        ),
        backgroundColor: Colors.transparent,
      ),
    );
  }
}

class SubcategoriesView extends StatefulWidget {
  MapViewModel model;

  bool isExpanded = true;

  SubcategoriesView({@required this.model});

  @override
  State<SubcategoriesView> createState() => _SubcategoriesViewState();
}

class _SubcategoriesViewState extends State<SubcategoriesView> {
  @override
  Widget build(BuildContext context) {
    return MediaQuery.removePadding(
      context: context,
      removeTop: true,
      child: SingleChildScrollView(
        child: Column(
          children: [
            ExpansionPanelList(
              children: [
                ExpansionPanel(
                  headerBuilder: (context, isExpanded) {
                    return ListTile(
                      title: Text(
                        'Категории',
                        style: TextStyle(color: Colors.black),
                      ),
                    );
                  },
                  body: ListTile(
                    title: Container(
                      height: MediaQuery.of(context).size.height,
                      child: GridView.builder(
                          physics: ScrollPhysics(),
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 3,
                            childAspectRatio:
                                MediaQuery.of(context).size.width /
                                    (MediaQuery.of(context).size.height / 1.5),
                          ),
                          itemCount: widget.model
                              .getCategoryByTapPosition(
                                  widget.model.tabPosition)
                              .tables
                              .length,
                          itemBuilder: (BuildContext context, int index) {
                            var item = widget.model
                                .getCategoryByTapPosition(
                                    widget.model.tabPosition)
                                .tables[index];
                            return GestureDetector(
                              onTap: () {
                                setState(() {
                                  widget.isExpanded = false;
                                });
                                widget.model.selectTable(item.id);
                                widget.model.loadData().then((value) {
                                  setState(() {});
                                });
                                // Navigator.pop(context);
                              },
                              child: Container(
                                padding: EdgeInsets.all(12),
                                margin: EdgeInsets.all(12),
                                height: 30,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10),
                                      topRight: Radius.circular(10),
                                      bottomLeft: Radius.circular(10),
                                      bottomRight: Radius.circular(10)),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 2,
                                      blurRadius: 10,
                                      offset: Offset(
                                          0, 3), // changes position of shadow
                                    ),
                                  ],
                                ),
                                child: Center(
                                  child: Text(
                                    widget.model
                                        .getCategoryByTapPosition(
                                            widget.model.tabPosition)
                                        .tables[index]
                                        .name,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.black),
                                  ),
                                ),
                              ),
                            );
                          }),
                    ),
                    tileColor: Colors.white,
                  ),
                  canTapOnHeader: true,
                  isExpanded: widget.isExpanded,
                ),
              ],
              animationDuration: Duration(milliseconds: 500),
              expansionCallback: (int index, bool isExpanded) {
                setState(() {
                  widget.isExpanded = !isExpanded;
                });
              },
            ),
            widget.isExpanded || widget.model.valuesForChart.isEmpty
                ? Container()
                : Container(
                    height: MediaQuery.of(context).size.height,
                    child: _ChartView(
                      model: widget.model,
                    ),
                  )
          ],
        ),
      ),
    );
  }
}

/// Collection of Australia state code data.
class Model {
  /// Initialize the instance of the [Model] class.
  const Model(this.code, this.state, this.color, this.stateCode);

  /// Represents the Australia state name.
  final String state;

  /// Represents the Australia state color.
  final Color color;

  /// Represents the Australia state code.
  final String stateCode;

  final int code;
}

class _ChartView extends StatefulWidget {
  MapViewModel model;

  _ChartView({this.model});

  @override
  _ChartViewState createState() => _ChartViewState();
}

class _ChartViewState extends State<_ChartView> {
  List<Datum> data = [];

  @override
  void initState() {
    super.initState();

    data = widget.model.valuesForChart;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(children: [
      //Initialize the chart widget
      SfCartesianChart(
          primaryXAxis: CategoryAxis(),
          // Chart title
          // title: ChartTitle(text: 'Half yearly sales analysis'),
          // Enable legend
          legend: Legend(isVisible: true),
          // Enable tooltip
          tooltipBehavior: TooltipBehavior(enable: true),
          series: <ChartSeries<Datum, String>>[
            LineSeries<Datum, String>(
                dataSource: data,
                xValueMapper: (Datum sales, _) => sales.period.name,
                yValueMapper: (Datum sales, _) => double.parse(sales.value),
                name: '',
                // Enable data label
                dataLabelSettings: DataLabelSettings(isVisible: true))
          ]),
    ]));
  }
}

class ExpandableCardContainer extends StatefulWidget {
  final bool isExpanded;
  final Widget collapsedChild;
  final Widget expandedChild;

  const ExpandableCardContainer(
      {Key key, this.isExpanded, this.collapsedChild, this.expandedChild})
      : super(key: key);

  @override
  _ExpandableCardContainerState createState() =>
      _ExpandableCardContainerState();
}

class _ExpandableCardContainerState extends State<ExpandableCardContainer> {
  @override
  Widget build(BuildContext context) {
    return new AnimatedContainer(
      duration: new Duration(milliseconds: 200),
      curve: Curves.easeInOut,
      child: widget.isExpanded ? widget.expandedChild : widget.collapsedChild,
    );
  }
}
