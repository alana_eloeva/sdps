const String AddingDataFirstStepRoute = "AddingDataFirstStep";
const String AddingDataSecondStepRoute = "AddingDataSecondStep";

const String DataAnalysisRegionsRoute = "DataAnalysisRegions";
const String DataAnalysisDataRoute = "DataAnalysisData";
const String DataAnalysisPeriodsRoute = "DataAnalysisPeriods";
const String DataAnalysisAlgorithmRoute = "DataAnalysisAlgorithm";
const String DataAnalysisParamsRoute = "DataAnalysis";
const String MapRoute = "Map";

const String DataBaseRoute = "DataBase";

