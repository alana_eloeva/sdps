import 'package:get_it/get_it.dart';
import 'package:sdps/services/data_service.dart';
import 'package:sdps/services/dialog_service.dart';
import 'package:sdps/services/navigation_service.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => DialogService());
}
