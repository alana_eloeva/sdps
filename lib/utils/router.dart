import 'package:flutter/material.dart';
import 'package:sdps/ui/adding_data/step_first.dart';
import 'package:sdps/ui/adding_data/step_second.dart';
import 'package:sdps/ui/data_analysis/data_analysis_params.dart';
import 'package:sdps/ui/data_analysis/data_analysis_algorithm.dart';
import 'package:sdps/ui/data_analysis/data_analysis_data.dart';
import 'package:sdps/ui/data_analysis/data_analysis_periods.dart';
import 'package:sdps/ui/data_analysis/data_analysis_regions.dart';
import 'package:sdps/ui/db/data_base.dart';
import 'package:sdps/ui/map/map.dart';
import 'package:sdps/utils/route_names.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case '/':
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: AddingDataFirstStepView(),
      );

    case AddingDataFirstStepRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: AddingDataFirstStepView(),
      );

    case AddingDataSecondStepRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: AddingDataSecondStepView(),
      );

    case DataBaseRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: DataBase(),
      );

    default:
      return MaterialPageRoute(
        builder: (_) => Scaffold(
          body: Center(child: Text('No route defined for ${settings.name}')),
        ),
      );
  }
}

PageRoute _getPageRoute({String routeName, dynamic args, Widget viewToShow}) {
  return MaterialPageRoute(
      settings: RouteSettings(name: routeName, arguments: args),
      builder: (_) => viewToShow);
}

Route<dynamic> generateRoute2(RouteSettings settings) {
  switch (settings.name) {
    case '/':
      return _getPageRoute2(
        settings: settings,
        page: AddingDataFirstStepView(),
      );

    case AddingDataFirstStepRoute:
      return _getPageRoute2(
        settings: settings,
        page: AddingDataFirstStepView(),
      );

    case AddingDataSecondStepRoute:
      return _getPageRoute2(
        settings: settings,
        page: AddingDataSecondStepView(),
      );

    case DataBaseRoute:
      return _getPageRoute2(
        settings: settings,
        page: DataBase(),
      );

    case DataAnalysisRegionsRoute:
      return _getPageRoute2(
        settings: settings,
        page: DataAnalysisRegionsView(),
      );

    case DataAnalysisParamsRoute:
      return _getPageRoute2(
        settings: settings,
        page: DataAnalysisParamsView(),
      );

    case DataAnalysisDataRoute:
      return _getPageRoute2(
        settings: settings,
        page: DataAnalysisDataView(),
      );

    case DataAnalysisPeriodsRoute:
      return _getPageRoute2(
        settings: settings,
        page: DataAnalysisPeriodsView(),
      );

    case DataAnalysisAlgorithmRoute:
      return _getPageRoute2(
        settings: settings,
        page: DataAnalysisAlgorithmView(),
      );

    case MapRoute:
      return _getPageRoute2(settings: settings, page: MapView());

    default:
      return MaterialPageRoute(
        builder: (_) => Scaffold(
          body: Center(child: Text('No route defined for ${settings.name}')),
        ),
      );
  }
}

PageRoute _getPageRoute2({RouteSettings settings, Widget page}) {
  return PageRouteBuilder(
      settings: settings,
      pageBuilder: (_, __, ___) => page,
      transitionsBuilder: (_, anim, __, child) {
        return FadeTransition(
          opacity: anim,
          child: child,
        );
      });
}

Widget getPageWidget(RouteSettings settings) {
  if (settings.name == null) {
    return null;
  }
  final uri = Uri.parse(settings.name);
  switch (uri.path) {
    case '/':
      return AddingDataFirstStepView();

    case AddingDataFirstStepRoute:
      return AddingDataFirstStepView();

    case AddingDataSecondStepRoute:
      return AddingDataSecondStepView();

    case DataAnalysisRegionsRoute:
      return DataAnalysisRegionsView();

    case DataAnalysisParamsRoute:
      return DataAnalysisParamsView();

    case DataAnalysisDataRoute:
      return DataAnalysisDataView();

    case DataAnalysisPeriodsRoute:
      return DataAnalysisRegionsView();

    case DataAnalysisAlgorithmRoute:
      return DataAnalysisAlgorithmView();

    case DataBaseRoute:
      return DataBase();

    default:
      return AddingDataFirstStepView();
  }
}
