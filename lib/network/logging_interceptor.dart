// ignore_for_file: avoid_print, unnecessary_string_escapes

import 'dart:convert';

import 'package:dio/dio.dart';

class LoggingInterceptor extends Interceptor {
  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    print("""ERROR:
URL: ${err.requestOptions.uri}\n
Method: ${err.requestOptions.method}
Headers: ${json.encode(err.response?.headers.map)}
StatusCode: ${err.response?.statusCode}
Data: ${json.encode(err.response?.data)}
<— END HTTP
""");
    return super.onError(err, handler);
  }

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    print("""REQUEST:
${cURLRepresentation(options)}
""");
    return super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    print("""RESPONSE:
URL: ${response.requestOptions.uri}
Method: ${response.requestOptions.method}
Headers: ${json.encode(response.requestOptions.headers)}
Data: ${json.encode(response.data)}
""");
    return super.onResponse(response, handler);
  }

  String cURLRepresentation(RequestOptions options) {
    List<String> components = ["\$ curl -i"];
    if (options.method.toUpperCase() == "GET") {
      components.add("-X ${options.method}");
    }

    options.headers.forEach((k, v) {
      if (k != "Cookie") {
        components.add("-H \"$k: $v\"");
      }
    });

    var data = json.encode(options.data);
    data = data.replaceAll('\"', '\\\"');
    components.add("-d \"$data\"");

    components.add("\"${options.uri.toString()}\"");

    return components.join('\\\n\t');
  }
}
