import 'dart:io';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class DioClient {
  Dio dio;
  static const baseURL = 'http://192.168.43.133:8000/api/';

  DioClient() {
    dio = Dio(BaseOptions(baseUrl: baseURL));
    if (!kIsWeb) {
      (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
          (HttpClient client) {
        client.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;
        return client;
      };
    }
    dio.options.headers["accept"] = "application/json";
    dio.options.headers["Access-Control-Allow-Origin"] = "*";
    dio.options.headers["Access-Control-Allow-Credentials"] = true;
    dio.options.headers["Access-Control-Allow-Headers"] = "Origin,Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,locale";
    dio.options.headers["Access-Control-Allow-Methods"] = "GET, HEAD";

    // dio.interceptors.add(LoggingInterceptor());
  }

  Future<Response> post({
    @required String endpoint,
    Map<String, dynamic> body = const {},
  }) async {
    Response response;
    try {
      response = await dio.post(endpoint, data: body);
    } on DioError catch (e) {
      throw Failed(error: e.toString());
    }
    return response;
  }

  Future<Response> get(
      {@required String endpoint, Map<String, dynamic> body = const {}}) async {
    Response response;
    try {
      response = await dio.get(endpoint);
    } on DioError catch (e) {
      throw Failed(error: e.toString());
    }

    return response;
  }
}

class AuthenticationFailed implements Exception {
  final String error;

  AuthenticationFailed(this.error);
}

class NoStoreSelected implements Exception {}

class RefreshToken implements Exception {}

class InvalidJWTToken implements Exception {}

class JwtTokenRevoked implements Exception {}

class NotFoundBarcode implements Exception {}

class Failed implements Exception {
  String error;

  Failed({this.error = ''});
}
