const String app_name = "Dashboard Kit";

//MENU
const String menu_first = "База данных";
const String menu_second = "Добавление данных";
const String menu_third = "Анализ данных";

//APPBAR
const String appbar_adding_data_step1 = "Добавление данных. Шаг 1";
const String appbar_adding_data_step2 = "Добавление данных. Шаг 2";

const String appbar_analysis_region = "Выбор регионов для анализа";
const String appbar_analysis_data = "Выбор данных для анализа";
const String appbar_analysis_period = "Выбор периодов для анализа";
const String appbar_analysis_algorithm = "Выбор алгоритма для анализа";
const String appbar_analysis = "Анализ";
const String appbar_map = "Карта";

