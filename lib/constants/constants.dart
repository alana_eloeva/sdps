class Constants {
  static String url = "https://msofter.com/tanya/sdps/public/api/";

  static Map<String, String> headers = {
    "Content-Type": "application/json",
    "Accept": "application/json"
  };

  static String getCategoriesUrl() {
    return '${url}table';
  }

  static String getYearsUrl() {
    return '${url}period';
  }

  static String getRegionsUrl() {
    return '${url}region';
  }
}

enum DataSourceType { existing_table, new_table }

extension DataSourceTypeExtension on DataSourceType {
  String title() {
    switch (this) {
      case DataSourceType.existing_table:
        return 'Таблица из БД';
        break;
      case DataSourceType.new_table:
        return 'Добавить новую таблицу';
        break;
    }
  }
}

enum TableType { single_value, array_value }

extension TableTypeExtension on TableType {
  String title() {
    switch (this) {
      case TableType.single_value:
        return 'Единичное значение';
        break;
      case TableType.array_value:
        return 'Набор значений';
        break;
    }
  }
}
