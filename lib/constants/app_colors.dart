import 'dart:ui';

const Color backgroundColor = Color(0xFFf7f8fc);

const Color darkBlueColor = Color(0xFFB151E26);

const Color greyColor = Color(0xFF363740);
const Color lightGreyColor = Color(0xFFA4A6B3);


const Color blueColor = Color(0xFF0c7cff);
const Color blueColor2 = Color(0xFF3751FF);
const Color lightBlueColor = Color(0xFF89a4c7);
const Color blueColor3 = Color(0xFF1374be);

const Color darkGreenColor = Color(0xFF25a996);
const Color greenColor = Color(0xFF15d7b5);
const Color greenColor2 = Color(0xFF5bd8d0);

const Color orangeColor = Color(0xFFe97735);
const Color lightOrangeColor = Color(0xFFf3be98);

const Color purpleColor = Color(0xFFd176ed);
const Color lightPurpleColor = Color(0xFFdfdfeb);

const Color darkGreyColor = Color(0xFF45484b);
const Color lightGreyColor3 = Color(0xFF9FA2B4);
const Color lightGreyColor2 = Color(0xFFedf2f7);

