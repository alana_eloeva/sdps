import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'app_colors.dart';

TextStyle textStyle_header1 =
    TextStyle(color: greyColor, fontSize: 20, fontWeight: FontWeight.w600);

TextStyle textStyle_header2 =
    TextStyle(color: greyColor, fontSize: 18, fontWeight: FontWeight.w600);

TextStyle textStyle_header3 =
    TextStyle(color: greyColor, fontSize: 14, fontWeight: FontWeight.w600);

TextStyle textStyle_header3_grey =
    TextStyle(color: lightGreyColor, fontSize: 14, fontWeight: FontWeight.w500);

TextStyle textStyle_underline = TextStyle(
    decoration: TextDecoration.underline,
    color: Colors.black,
    fontSize: 18,
    fontWeight: FontWeight.w500);

BoxDecoration buttonDecoration = BoxDecoration(
  borderRadius: BorderRadius.circular(8.0),
  color: blueColor2,
);

setTextFieldDecoration_withIcon(
    {@required String hint, @required Icon suffixIcon}) {
  return InputDecoration(
    hintText: hint,
    suffixIcon: suffixIcon,
    enabledBorder: UnderlineInputBorder(
      borderSide: BorderSide(color: lightGreyColor2),
    ),
    focusedBorder: UnderlineInputBorder(
      borderSide: BorderSide(color: blueColor2),
    ),
  );
}

setTextFieldDecoration({@required String hint}) {
  return InputDecoration(
    hintText: hint,
    enabledBorder: UnderlineInputBorder(
      borderSide: BorderSide(color: lightGreyColor2),
    ),
    focusedBorder: UnderlineInputBorder(
      borderSide: BorderSide(color: blueColor2),
    ),
  );
}
