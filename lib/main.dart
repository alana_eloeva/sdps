import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_admin_scaffold/admin_scaffold.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:sdps/services/dialog_service.dart';
import 'package:sdps/services/navigation_service.dart';

import 'package:sdps/utils/locator.dart';
import 'package:sdps/utils/route_names.dart';
import 'package:sdps/utils/router.dart';

import 'constants/app_colors.dart';

import 'package:window_size/window_size.dart' as window_size;
import 'dart:math' as math;

import 'managers/dialog_manager.dart';
import 'models/db_table_groups.dart';

Box box;

void main() async {
  await Hive.initFlutter();
  var path = Directory.current.path;
  Hive
    ..init(path)
    ..registerAdapter(CategoriesResponseAdapter())
    ..registerAdapter(CategoryAdapter())
    ..registerAdapter(TableAdapter());
  box = await Hive.openBox('data');

  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();
  window_size.getWindowInfo().then((window) {
    final screen = window.screen;
    if (screen != null) {
      final screenFrame = screen.visibleFrame;
      final width = math.max((screenFrame.width).roundToDouble(), 800.0);
      final height = math.max((screenFrame.height).roundToDouble(), 600.0);
      final left = ((screenFrame.width - width)).roundToDouble();
      final top = ((screenFrame.height - height)).roundToDouble();
      final frame = Rect.fromLTWH(left, top, width, height);
      window_size.setWindowFrame(frame);
      window_size.setWindowMinSize(Size(0.8 * width, 0.9 * height));
      window_size.setWindowMaxSize(Size(1.5 * width, 1.5 * height));
      window_size.setWindowTitle('SDPS ${Platform.operatingSystem}');
    }
  });
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: (context, child) => Navigator(
        key: locator<DialogService>().dialogNavigationKey,
        onGenerateRoute: (settings) => MaterialPageRoute(
          builder: (context) => DialogManager(child: child),
        ),
      ),
      navigatorKey: locator<NavigationService>().navigationKey,
      onGenerateRoute: generateRoute2,
      debugShowCheckedModeBanner: false,
      theme:
          ThemeData.light().copyWith(scaffoldBackgroundColor: backgroundColor),
    );
  }
}

class MyScaffold extends StatelessWidget {
  const MyScaffold({
    Key key,
    @required this.route,
    @required this.body,
  }) : super(key: key);

  final Widget body;
  final String route;

  final List<MenuItem> _sideBarItems = const [
    MenuItem(
      title: 'База данных',
      route: DataBaseRoute,
      icon: Icons.dashboard,
    ),
    MenuItem(
      title: 'Добавление данных',
      route: AddingDataFirstStepRoute,
      icon: Icons.add_chart,
    ),
    MenuItem(
      title: 'Анализ данных',
      icon: Icons.stacked_line_chart,
      route: DataAnalysisRegionsRoute,
    ),
    MenuItem(
      title: 'Карта',
      icon: Icons.add_location_rounded,
      route: MapRoute,
    ),
  ];

  final List<MenuItem> _adminMenuItems = const [
    MenuItem(
      title: 'Профиль',
      icon: Icons.account_circle,
      route: '/',
    ),
    MenuItem(
      title: 'Настройки',
      icon: Icons.settings,
      route: '/',
    ),
    MenuItem(
      title: 'Выход',
      icon: Icons.logout,
      route: '/',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return AdminScaffold(
        backgroundColor: backgroundColor,
        appBar: AppBar(
          title: const Text('Анализ и обработка статистических данных'),
          backgroundColor: greyColor,
          actions: [
            PopupMenuButton<MenuItem>(
              child: Container(
                  padding: EdgeInsets.only(right: 14),
                  child: const Icon(Icons.account_circle)),
              itemBuilder: (context) {
                return _adminMenuItems.map((MenuItem item) {
                  return PopupMenuItem<MenuItem>(
                    value: item,
                    child: Row(
                      children: [
                        Icon(
                          item.icon,
                          color: greyColor,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text(
                            item.title,
                            style: TextStyle(
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                }).toList();
              },
              onSelected: (item) {
                print(
                    'actions: onSelected(): title = ${item.title}, route = ${item.route}');
                Navigator.of(context).pushNamed(item.route);
              },
            )
          ],
        ),
        sideBar: SideBar(
          backgroundColor: greyColor,
          activeBackgroundColor: lightGreyColor.withOpacity(0.5),
          borderColor: greyColor,
          iconColor: lightGreyColor,
          activeIconColor: Colors.white,
          textStyle: TextStyle(color: lightGreyColor, fontSize: 16),
          activeTextStyle: TextStyle(color: Colors.white, fontSize: 16),
          items: _sideBarItems,
          selectedRoute: route,
          onSelected: (item) {
            print(
                'sideBar: onTap(): title = ${item.title}, route = ${item.route}');
            if (item.route != null && item.route != route) {
              Navigator.of(context).pushNamed(item.route);
            }
          },
        ),
        body: body);
  }
}
