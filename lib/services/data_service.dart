import 'package:sdps/models/data.dart';
import 'package:sdps/models/db_table_groups.dart';
import 'package:sdps/network/dio_client.dart';

class DataService {
  final DioClient _dioClient = DioClient();

  Future<CategoriesResponse> getCategories() async {
    var response = await _dioClient.get(endpoint: 'table-groups/');
    return CategoriesResponse.fromJson(response.data);
  }

  // 20,6,15,7,5,26,9 - regions
  Future<DataResponse> getData(int region, int table, int period) async {
    var response = await _dioClient.get(
        endpoint:
            'data/?region=[$region]&period=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40]&table=[$table]');
    return DataResponse.fromJson(response.data);
  }
}
