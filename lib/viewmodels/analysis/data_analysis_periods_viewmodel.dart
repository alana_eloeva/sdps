import 'package:sdps/models/years.dart';
import 'package:sdps/viewmodels/base_viewmodel.dart';

class DataAnalysisPeriodsViewModel extends BaseViewModel {
  List<Year> _years = getYears();
  List<Year> get years => _years;

  onCheckYear(int index, bool value) {
    _years[index].selected = value;
    notifyListeners();
  }
}
