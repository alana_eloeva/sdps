import 'package:sdps/models/algorithm.dart';
import 'package:sdps/viewmodels/base_viewmodel.dart';

class DataAnalysisAlgorithmViewModel extends BaseViewModel {

  List<Algorithm> _algorithms = getAlgorithms();

  List<Algorithm> get algorithms => _algorithms;

  onCheckAlgorithm(int index, bool value){
    _algorithms[index].selected = value;
    notifyListeners();
  }
}
