import 'package:sdps/models/regions.dart';
import 'package:sdps/viewmodels/base_viewmodel.dart';


class DataAnalysisRegionsViewModel extends BaseViewModel {

  List<Region> _regions = getRegions();

  List<Region> get regions => _regions;

  onCheckRegion(int index, bool value) {
    _regions[index].selected = value;
    notifyListeners();
  }
}

