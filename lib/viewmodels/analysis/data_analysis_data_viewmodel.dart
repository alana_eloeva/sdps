import 'package:sdps/models/db_table_groups.dart';
import 'package:sdps/viewmodels/base_viewmodel.dart';

class DataAnalysisDataViewModel extends BaseViewModel {

  List<Category> _tables = getTables();
  List<Category> get tables => _tables;


  onCheckRegion(int index, bool value) {
    _tables[index].selected = value;
    notifyListeners();
  }

  onCheckNormalize(int index, bool value){
    _tables[index].normalize = value;
    notifyListeners();
  }
  
}
