import 'package:flutter/material.dart';
import 'package:sdps/models/data.dart';
import 'package:sdps/models/db_table_groups.dart';
import 'package:sdps/services/data_service.dart';

import '../../main.dart';
import '../base_viewmodel.dart';

class MapViewModel extends BaseViewModel {
  DataService service = DataService();
  List<Category> categoryList = [];
  List<Tab> tabs = [];
  List<Datum> valuesForChart = [];

  int tabPosition = 0;

  int _selectedRegion;
  int _selectedTable;

  loadCategories() async {
    setBusy(true);
    try {
      CategoriesResponse response = await service.getCategories();
      setBusy(false);
      if (response != null) {
        await box.put("data_response", response);
        categoryList = response.data;
        for (var item in categoryList) {
          tabs.add(Tab(text: item.name));
        }
      }
    } catch (err) {
      print(err);
    }
  }

  Future loadData() async {
    if (_selectedRegion == null || _selectedTable == null) return;
    setBusy(true);
    valuesForChart = [];
    try {
      DataResponse response =
          await service.getData(_selectedRegion, _selectedTable, 36);
      setBusy(false);
      if (response != null) {
        valuesForChart = response.data;
      }
    } catch (err) {
      print(err);
    }
  }

  Category getCategoryByTapPosition(int position) {
    if (tabs.length == 0) return null;
    if (categoryList.length == 0) return null;
    return categoryList[position];
  }

  getCategories() async {
    var response = await box.get("data_response");
    if (response != null) {
      categoryList = response.data;
      for (var item in categoryList) {
        tabs.add(Tab(text: item.name));
      }
    } else {
      loadCategories();
    }
  }

  tabPositionChanged(int index) {
    tabPosition = index;
  }

  selectRegion(int id) {
    _selectedRegion = id;
  }

  selectTable(int id) {
    _selectedTable = id;
  }
}
