
import 'package:flutter/material.dart';

enum LoadingStatus {
  completed,
  loading,
  empty
}

class BaseViewModel extends ChangeNotifier  {

  bool _busy = false;
  bool get busy => _busy;

  void setBusy(bool value) {
    _busy = value;
    notifyListeners();
  }
}