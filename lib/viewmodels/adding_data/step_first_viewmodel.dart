import 'package:flutter/cupertino.dart';
import 'package:sdps/constants/constants.dart';
import '../base_viewmodel.dart';

class AddingDataFirstStepViewModel extends BaseViewModel {
  DataSourceType _dataSourceType = DataSourceType.existing_table;
  TableType _tableType = TableType.single_value;

  bool _secondContainerVisibility = false;
  bool _thirdContainerVisibility = false;

  onChangeDataSourceType(DataSourceType value) {
    _dataSourceType = value;
    notifyListeners();
  }

  onChangeTableType(TableType value) {
    _tableType = value;
    notifyListeners();
  }

  DataSourceType getCurrentDataSourceTypeValue() {
    return _dataSourceType;
  }

  TableType getCurrentTableTypeValue() {
    return _tableType;
  }

  bool createNewTableTextFieldVisibility() {
    return _dataSourceType != DataSourceType.new_table;
  }

  showSecondContainer() {
    if (_secondContainerVisibility == true) return;
    _secondContainerVisibility = true;
    notifyListeners();
  }

  showThirdContainer() {
    if (_thirdContainerVisibility == true) return;
    _thirdContainerVisibility = true;
    notifyListeners();
  }

  getSecondContainerVisibility() {
    return _secondContainerVisibility;
  }

  getThirdContainerVisibility() {
    return _thirdContainerVisibility;
  }

  bool isSingleTableType() {
    return _tableType == TableType.single_value;
  }

  List<String> newFields = [];
  String singleFieldValue = '';
  String multipleFieldsValues = '';

  onSingleFieldValueChanged(String value) {
    singleFieldValue = value;
  }

  onMultipleFieldsValuesChanged(String value) {
    multipleFieldsValues = value;
  }

  addNewField({@required String name}) {
    newFields.add(name);
    notifyListeners();
  }

  onAddFieldButtonClicked(
      Function(List<String> regions, List<String> fields) navigate) {
    if (singleFieldValue.isEmpty && multipleFieldsValues.isEmpty) return;
    switch (_tableType) {
      case TableType.single_value:
        newFields.add(singleFieldValue);
        break;
      case TableType.array_value:
        List<String> values =
            multipleFieldsValues.replaceAll(' ', '').split(',');
        for (int i = 0; i < values.length; i++) {
          newFields.add(values[i]);
        }
        break;
    }

    navigate(_onLoadRegionList(), newFields);
  }

  List<String> _onLoadRegionList() {
    return [
      "Республика Дагестан",
      "Республика Ингушетия",
      "Кабардино-Балкарская Республика",
      "Карачаево-Черкесская Республика",
      "Республика Северная Осетия – Алания",
      "Чеченская Республика",
      "Ставропольский край",
    ];
  }
}
