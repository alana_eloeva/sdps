import 'package:flutter/foundation.dart';
import 'package:sdps/viewmodels/base_viewmodel.dart';

class AddingDataSecondStepViewModel extends BaseViewModel {
  DateTime _selectedDate = DateTime.now();
  int row;
  int column;
  var table2dArray;

  AddingDataSecondStepViewModel({@required int row, @required int column}) {
    this.row = row;
    this.column = column;
    table2dArray = List.generate(
        row, (i) => List.filled(column, '', growable: false),
        growable: false);
  }

  String get selectedYearForShow => _selectedDate.year.toString();

  DateTime get selectedDate => _selectedDate;

  onChangedSelectedDate(DateTime value) {
    _selectedDate = value;
    notifyListeners();
  }

  onCellChanged(String value, int i, int j) {
    table2dArray[i][j] = value;
  }
}
